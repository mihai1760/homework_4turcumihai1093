import React, {Component} from 'react'

class RobotForm extends Component{
    constructor(props){
        super(props)

        this.state={
            name: '',
            type: '',
            mass: ''
        }
        this.handleChange = (e) => {
            this.setState({
                [e.target.name]: e.target.value
            })
        }
        this.onAdd = this.onAdd.bind(this)

    }

    onAdd() {
        this.props.onAdd(this.state.name, this.state.type, this.state.mass)
    }


    render(){
        return <form> 
            <input id='name' type='text' name='name' onChange={this.handleChange}></input>
            <input id='type' type='text' name='type' onChange={this.handleChange}></input>
            <input id='mass' name='mass' onChange={this.handleChange}></input>
            <button type='submit' value='add' onClick={this.onAdd}>Add robot</button>
        </form>
    }
}

export default RobotForm